var config = {};
var builder = require('gcp-container-builder')(config);

var cloudbuild = Object.create(null);
cloudbuild.steps = [
	{
    	name: 'gcr.io/cloud-builders/gsutil',
    	args: ['cp',
    		'gs://trigger-cloud-build/*',
    		'.']
	}
	, {
		name: 'gcr.io/cloud-builders/gcloud',
		args: [
			'kms', 
			'decrypt', 
			'--ciphertext-file=.git-credentials.enc', 
			'--plaintext-file=.git-credentials',
			'--location=us-east4',
			'--key=deploy-key',
			'--keyring=gitlab-deploy-key']
	}
  	, {
		name: 'gcr.io/cloud-builders/git',
		args: [
		  'config',
		  '--global',
		  'credential.helper',
		  '\'store\''],
		  env: ['HOME=/workspace']
    }
	, {
		name: 'gcr.io/cloud-builders/git',
  		args: [
  		'clone',
  		'--depth',
  		'1',
  		'https://gitlab.com/joshlambert/trigger-container-builder.git'],
  		env: ['HOME=/workspace']
	}
	, {
		name: 'gcr.io/cloud-builders/docker',
  		args: [ 'build',
  		'-t',
  		'gcr.io/$PROJECT_ID/trigger-container-builder',
  		'./trigger-container-builder']
	}];

cloudbuild.images = ['gcr.io/$PROJECT_ID/trigger-container-builder'];

exports.get = (req, res) => {

	builder.createBuild(cloudbuild, function(err, resp) {
	    console.log(err);
	    console.log(resp);
	});

	res.status(200).send('Triggered.');

};